const Discord = require("discord.js");
const client = new Discord.Client();
const { token } = require("./config.json")
const db = require('megadb')
let prefix_db = new db.crearDB("prefixes");
let bienvenida = new db.crearDB("bienvenida")

client.on("ready", () => {
  console.log('Estoy listo.')
});

client.on('message', async message => {

  let prefix = "w!"

  if(message.channel.type === "dm") return;
  if (!message.content.startsWith(prefix)) return; 
  if (message.author.bot) return;
  const args = message.content.slice(prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

let cooldown = new Set()

if(cooldown.has(message.author.id)){
  message.channel.send(`<:config:619548761474990090> || **${message.author.username}**, espera un **poco** antes de ejecutar otro **comandos**.`)
  return;
}

const { readdirSync } = require('fs');
client.comandos = new Discord.Collection();

for(let comandos of readdirSync(`./comandos/`).filter(cmd => cmd.endsWith(".js"))) {

const comando = require(`./comandos/${comandos}`);
client.comandos.set(comando.nombre, comando);

}

cooldown.add(message.author.id);

setTimeout(() => {
  cooldown.delete(message.author.id);
}, 5000);

const cmd = client.comandos.get(command) || client.comandos.find(c => c.alias.includes(command));
if(!cmd) return;
cmd.run(client, message, args, prefix, db)
  

})

client.on('guildCreate', async guild => {
  console.log('Nuevo servidor ' + guild.id + ' | Owner name: ' + guild.owner.nickname + ' | Owner id: ' + guild.owner.id)
})

client.on('guildMemberAdd', async member => {
  let bienvenidamsg = await bienvenida.obtener('msg_'+member.guild.id)
  let bienvenidacanal = await bienvenida.obtener('canal_'+member.guild.id)
  let bienvenidapriv = await bienvenida.obtener('canalpriv_'+member.guild.id)
  let bienvenidaprivmsg = await bienvenida.obtener('msgpriv_'+member.guild.id)
  if(!bienvenidacanal) return;
  if(!bienvenidamsg) return;
  let mensajetxt = bienvenidamsg
  .replace(/{guild}/gi, member.guild.name)
  .replace(/{member}/gi, `<@${member.user.id}>`)
  .replace(/{name}/gi, member.displayName)
  .replace(/{users}/gi, member.guild.memberCount)
  let bc = client.channels.get(bienvenidacanal.id)
  bc.send(mensajetxt)
  if(!bienvenidapriv) return;
  if(!bienvenidaprivmsg) return;
  let mensajetxtpriv = bienvenidaprivmsg
  .replace(/{guild}/gi, member.guild.name)
  .replace(/{member}/gi, `<@${member.user.id}>`)
  .replace(/{name}/gi, member.displayName)
  .replace(/{users}/gi, member.guild.memberCount)
  client.users.get(member.id).send(mensajetxtpriv)
})

client.on ('unhandledRejection', (error) => console.error('Uncaught Promise Rejection', error));
client.on ('error', (error) => console.error(error));
client.on ('warn', (error) => console.warn(error));

client.login(token)