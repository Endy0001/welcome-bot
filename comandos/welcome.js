const Discord = require("discord.js")
module.exports = {
   nombre: 'welcome',
   alias: ["wl"],
   descripcion: 'Bienvenida del servidor',
   run: async (client, message, args, prefix, db) => {
    let bienvenida = new db.crearDB("bienvenida")
    let toggle = new db.crearDB('toggle');
    let cmd = args[0]

 if(cmd === "mensaje"){
     if(!bienvenida.tiene('canal_' + message.guild.id)) return message.channel.send(`<:servidor:619177679983345664> **${message.author.username}**, detecte que no tienes el canal colocado.`)
     let mensajeb = args.slice(1).join(' ');
     if(!mensajeb) return message.channel.send(`<:completo:619177679706390548> **${message.author.username}**, por favor, coloque un mensaje.\n<:servidor:619177679983345664> Mira algunos ejemplos, usa: **${prefix}welcome ayuda**`);
     await bienvenida.establecer('msg_'+message.guild.id, mensajeb)
     let mensajetxt = mensajeb
     .replace(/{guild}/gi, message.guild.name)
     .replace(/{member}/gi, message.author)
     .replace(/{name}/gi, message.displayName)
     .replace(/{users}/gi, message.guild.memberCount)
     message.channel.send(`<:completo:619177679706390548> **${message.author.username}**, mensaje de bienvenida: \`${mensajeb}\` fue colocado correctamente.\n**Preview**: ${mensajetxt}`)

     
    } else if(cmd === "ayuda") {
     var embed = new Discord.RichEmbed()
     if(bienvenida.tiene('canal_' + message.guild.id)){
         embed.setTitle('<:servidor:619177679983345664> || Sistema de bienvenida: <:encendido:618840446063345685> **Activado**')
     } else {
        embed.setTitle('<:servidor:619177679983345664> || Sistema de bienvenida: <:apagado:618840445887447070> **Desactivado**')
     }
     if(bienvenida.tiene('msg_' + message.guild.id)){
         let msgbv = await bienvenida.obtener('msg_' + message.guild.id);
         embed.addField('**▸ Mensaje de bienvenida:**', msgbv)
     } else {
        embed.addField('**▸ Mensaje de bienvenida:**', 'Ningún mensaje definido.')
     }
     if(bienvenida.tiene('canalpriv_' + message.guild.id)){
        let msgpriv = await bienvenida.obtener('canalpriv_' + message.guild.id);
        embed.addField('**▸ Mensaje privado:**', '<:encendido:618840446063345685> **Activado**')
     } else {
        embed.addField('**▸ Mensaje privado:**', '<:apagado:618840445887447070> **Desactivado**')
     }
     if(bienvenida.tiene('msgpriv_' + message.guild.id)){
        let msgpriv = await bienvenida.obtener('msgpriv_' + message.guild.id);
        embed.addField('**▸ Mensaje privado colocado:**', msgpriv)
     } else {
        embed.addField('**▸ Mensaje privado colocado:**', 'Ningún mensaje definido.')
     }
     if(bienvenida.tiene('canal_' + message.guild.id)){
         let x = await bienvenida.obtener('canal_' + message.guild.id)
         
        embed.addField('**▸ Canal colocado:**', `<#${x.id}>`)
    } else {
       embed.addField('**▸ Canal colocado:**', 'Ningún canal definido.')
    }
    embed.addField('**▸ ¿Cómo usar?**', `**${prefix}welcome mensaje** \`{member} entro, ¡bienvenido!\``)
    embed.addField('**▸ Variables:**', '```{member} - Menciona al usuario\n{guild} - Muestra el nombre del servidor\n{name} - Nombre del usuario\n{users} - Cantidad de miembros en el servidor```')
    embed.addField('**▸ Ejemplos:**', `\`1.\` *{member}* - Hola ${message.author}.\n\`2.\` *{guild}* - ¡Entraste al servidor **${message.guild.name}**!\n\`3.\` *{name}* - Bienvenido **${message.author.username}**\n\`4.\` *{users}* - Ahora somos **${message.guild.memberCount}**`)
    embed.setFooter(message.author.tag, message.author.displayAvatarURL)
    message.channel.send(embed)
   } else if(cmd === "colocar"){
      let canal = message.mentions.channels.first();
      if(canal){
      await bienvenida.establecer('canal_'+message.guild.id, canal)
        message.channel.send(`<:completo:619177679706390548> **${message.author.username}**, el canal de bienvenida fue colocado para el canal ${canal}`)
      } else if(message.channel){
         await bienvenida.establecer('canal_'+message.guild.id, message.channel)
         message.channel.send(`<:completo:619177679706390548> **${message.author.username}**, el canal de bienvenida fue colocado correctamente en ${message.channel}`)
      }
    } else if(cmd === "ejemplo"){
       if(bienvenida.tiene('msg_'+message.guild.id)) {
        let bienvenidamsg = await bienvenida.obtener('msg_'+message.guild.id)
        let bienvenidacanal = await bienvenida.obtener('canal_'+message.guild.id)
        let bc = client.channels.get(bienvenidacanal.id)
        let mensajetxt = bienvenidamsg
        .replace(/{guild}/gi, message.guild.name)
        .replace(/{member}/gi, message.author)
        .replace(/{name}/gi, message.displayName)
        .replace(/{users}/gi, message.guild.memberCount)
        message.channel.send(`<:completo:619177679706390548> || **${message.author.username}**, ejemplo enviado para el canal definido.`)
        bc.send('**▸ Mensaje de bienvenida:** ' + mensajetxt)
      } else {
         let bienvenidacanal = await bienvenida.obtener('canal_'+message.guild.id)
         let bc = client.channels.get(bienvenidacanal.id)
         message.channel.send(`<:completo:619177679706390548> || **${message.author.username}**, ejemplo enviado para el canal definido.`)
         bc.send('**▸ Mensaje de bienvenida:** Ningún mensaje definido.')
       }
    } else if(cmd === "welcomedm"){
      if(!toggle.tiene(message.guild.id)) {
      message.channel.send(`<:servidor:619177679983345664> || **${message.author.username}**, ¡sistema de mensaje privado \`AL ENTRAR\` fueron activadas!`)
      await bienvenida.establecer('canalpriv_' + message.guild.id, "true");
      await toggle.establecer(message.guild.id, {"toggle": true})
      } else {
      message.channel.send(`<:servidor:619177679983345664> || **${message.author.username}**, ¡sistema de mensaje privado \`AL ENTRAR\` fueron desactivadas!`)
      await bienvenida.eliminar('canalpriv_' + message.guild.id);
      await toggle.eliminar(message.guild.id)
      }

    } else if(cmd === "mensajedm"){
      if(!bienvenida.tiene('canalpriv_'+message.guild.id)) return message.channel.send('<:error:619177679911911434> || ¡En el servidor el sistema de **Mensaje Privado** `AL ENTRAR` esta desactivado!')
      let mensajepriv = args.slice(1).join(' ');
      if(!mensajepriv) return message.channel.send(`<:error:619177679911911434> || **${message.author.username}**, necesitas colocar un mensaje.\n- Use: **${prefix}welcome** \`mensajedm <mensaje>\``);
      await bienvenida.establecer('msgpriv_'+message.guild.id, mensajepriv)
      message.channel.send(`<:completo:619177679706390548> || Mensaje alterado correctamente.`)
    }else if(cmd === "remover"){
       await bienvenida.eliminar('msgpriv_'+message.guild.id)
       await bienvenida.eliminar('canalpriv_'+message.guild.id)
       await bienvenida.eliminar('msg_'+message.guild.id)
       await bienvenida.eliminar('canal_'+message.guild.id)
       await toggle.eliminar(message.guild.id)
       message.channel.send(`<:error:619177679911911434> **${message.author.username}**, bienvenida removida correctamente.`)
    } else if(cmd){
       message.channel.send(`<:error:619177679911911434> | **${message.author.username}**, argumento invalido.`)
    } else {
       message.channel.send(`<:error:619177679911911434> | **${message.author.username}**, ¡usted esta utilizando el comando de forma incorrecta!\n<:database:619177679933145088> Use **${prefix}welcome** \`<colocar #chat || mensaje [msg] || remover || ejemplo || ayuda || welcomedm || mensajedm>\``)
    }

}
}